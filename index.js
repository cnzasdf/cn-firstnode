const createServer = require('http').createServer
const ifError = require('assert').ifError
const log = console.log.bind(console)
const port = process.env.port || 1337

const server = createServer((req, resp) => {
    log('got request')

    resp.setHeader('Content-Type', 'text/plain')
    resp.write('Your ip is: ')
    resp.write(`${req.socket.remoteAddress}:${req.socket.remotePort}`)
    resp.write(JSON.stringify(req.headers));
    resp.end()
})

server.listen(port, err => {
    ifError(err)
    log(`listening at ${port}`)
})
